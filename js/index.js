"use strict"

//1 and 2 -
//рядок можна створити за допомогою лапок, а саме:
//( '' ) || ( "" ) -різниці між цими рядками немає.
//( `` ) - ці лапки створюють шаблонний рядок, різниця в тому, що сюди можна зручно додавти шматки коду. й інтегрувати його з рядком.
//3-
//певно це буде зручніше зробити через .localeCompare() цей метод поверне нам число по якому уже ми побачимо результат
//4-
//date.now() - повертає нам таймстемп. тобто кількість мілесекунд, які пройшли з 1 січня 1970 року 00:00:00 сек. по utc
//5-
// date.now() - просто викликає таймстеп
// new Date() - фактично  створює обєкт дати. через нього можна викликати цілу купу методів, й по різному оперувати ними

//________________________________________________________________________________________________________________

//1-
const isPalindrome = (str) => {
    return str.split("").reverse().join("") === str;
}
console.log(isPalindrome('34543')); // === true
console.log(isPalindrome('723210')); // === false

//2-
const getLength = (string, length) => {
    return string.length <= length;
}
console.log(getLength("boooom", 5)) // (x-6) === false
console.log(getLength("booom", 5)) // (x-5) === true
console.log(getLength("boom", 5)) // (x-4) === true

//3-
const getUserAge = () => {
    let years = prompt("введіть свій рік народження");
    return new Date().getFullYear() - years
}
console.log(getUserAge());